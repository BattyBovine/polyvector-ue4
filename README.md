# PolyVector
PolyVector is a proof-of-concept for importing SWF animations and triangulating them into polygons rendered with the GPU. It is implemented as a component plugin for Unreal Engine 4.

Shape and path data is stored in memory using curves, which are tessellated and triangulated as necessary. Automatic LOD and intelligent point reduction are both available. Animation is supported, but is a work-in-progress.

[A short feature demonstration is available on YouTube.](https://www.youtube.com/watch?v=azgkyvVQJcs)

To be completed:
================
1. Masks are not supported, but will be a priority.
2. Gradients are not supported yet.
