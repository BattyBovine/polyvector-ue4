namespace UnrealBuildTool.Rules
{
	public class PolyVectorComponent : ModuleRules
	{
		public PolyVectorComponent(ReadOnlyTargetRules Target) : base(Target)
		{
			PCHUsage = ModuleRules.PCHUsageMode.NoSharedPCHs;
			PrivatePCHHeaderFile = "Public/IPolyVectorComponent.h";

			PublicIncludePaths.AddRange(
				new string[] {
				}
				);

			PrivateIncludePaths.AddRange(
				new string[] {
				}
				);

			PublicDependencyModuleNames.AddRange(
				new string[]
				{
					"Core",
					"CoreUObject",
					"Engine",
					"ProceduralMeshComponent"
				}
				);

			PrivateDependencyModuleNames.AddRange(
				new string[] {
				}
				);

			DynamicallyLoadedModuleNames.AddRange(
				new string[] {
				}
				);

			if (Target.bBuildEditor)
			{
				PrivateDependencyModuleNames.AddRange(
					new string[]
					{
						"MeshDescription",
						"MeshUtilities",
						"MessageLog"
					}
					);
			}
		}
	}
}
