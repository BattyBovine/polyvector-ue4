#include "Curve2D.h"

DEFINE_LOG_CATEGORY(LogCurve2D);


uint32 FCurve2D::GetPointCount() const
{
	return this->Points.Num();
}

void FCurve2D::AddPoint(const FVector2D &Pos, const FVector2D &In, const FVector2D &Out, int32 AtPos)
{
	FBezierPoint2D N;
	N.Position = Pos;
	N.In = In;
	N.Out = Out;
	if (AtPos >= 0 && AtPos < this->Points.Num())
	{
		this->Points.Insert(N, AtPos);
	}
	else
	{
		this->Points.Push(N);
	}
}

TArray<FVector2D> FCurve2D::GetData() const
{
	TArray<FVector2D> D;
	D.Reserve(this->Points.Num() * 3);

	for (int32 i = 0; i < this->Points.Num(); i++)
	{
		D[i * 3 + 0] = this->Points[i].In;
		D[i * 3 + 1] = this->Points[i].Out;
		D[i * 3 + 2] = this->Points[i].Position;
	}

	return D;
}

TArray<FVector2D> FCurve2D::GetPositionData() const
{
	TArray<FVector2D> Out;

	for (int32 i = 0; i < this->Points.Num(); i++)
	{
		Out.Add(this->Points[i].Position);
	}

	return Out;
}

TArray<FVector2D> FCurve2D::Tessellate(const uint32 CurvePointCount) const
{
	TArray<FVector2D> Tess;

	if (CurvePointCount <= 0)
	{
		for (const FBezierPoint2D& Point : this->Points)
		{
			Tess.Add(Point.Position);
		}
	}
	else
	{
		FBezierPoint2D Point1 = this->Points[0];
		const uint32 PointCount = this->Points.Num();
		for (uint32 i = 1; i < PointCount; i++)
		{
			const FBezierPoint2D& Point2 = this->Points[i];
			const FVector ControlPoints[4] =
			{
				FVector(Point1.Position.X, Point1.Position.Y, 0.0),
				FVector(Point1.Position.X + Point1.Out.X, Point1.Position.Y + Point1.Out.Y, 0.0),
				FVector(Point2.Position.X + Point2.In.X, Point2.Position.Y + Point2.In.Y, 0.0),
				FVector(Point2.Position.X, Point2.Position.Y, 0.0)
			};
			TArray<FVector> NewPointList;
			FVector::EvaluateBezier(ControlPoints, CurvePointCount + 2, NewPointList);
			for (const FVector& Point : NewPointList)
			{
				Tess.Add(FVector2D(Point.X, Point.Y));
			}
			Point1 = Point2;
		}
	}

	return Tess;
}
