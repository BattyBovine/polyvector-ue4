/**
 * Copyright ©2019 BattyBovine
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 **/


#include "PolyVectorAsset.h"


void UPolyVectorAsset::SetCachedMesh(const FPolyVectorMeshSection& MeshSection, const int8 Lod, const uint16 ID)
{
	if (!this->MeshCache.Contains(Lod))
	{
		this->MeshCache.Emplace(Lod);
	}
	this->MeshCache[Lod].Emplace(ID, MeshSection);
}

void UPolyVectorAsset::SetCachedMeshes(const FPolyVectorMeshes& Meshes, const int8 Lod)
{
	this->MeshCache.Emplace(Lod, Meshes);
}

FPolyVectorMeshSection UPolyVectorAsset::GetCachedMesh(const int8 Lod, const uint16 ID) const
{
	if (this->HasCachedMesh(Lod, ID))
	{
		return this->MeshCache[Lod][ID];
	}
	return FPolyVectorMeshSection();
}

bool UPolyVectorAsset::HasCachedMesh(const int8 Lod, const uint16 ID) const
{
	return (this->MeshCache.Contains(Lod) && this->MeshCache[Lod].Contains(ID));
}

bool UPolyVectorAsset::HasLod(const int8 Lod) const
{
	return this->MeshCache.Contains(Lod);
}

int8 UPolyVectorAsset::GetNearestLod(const int8 Lod) const
{
	TArray<int8> AvailableLods;
	this->MeshCache.GetKeys(AvailableLods);
	if (AvailableLods.Num() <= 0)
	{
		return INT8_MIN;
	}
	if (AvailableLods.Contains(Lod))
	{
		return Lod;
	}
	else
	{
		AvailableLods.Sort();
		uint8 MinSearchPosition = 0;
		uint8 MaxSearchPosition = AvailableLods.Num();
		uint8 SearchPosition = MaxSearchPosition / 2;
		while (true)
		{
			const int8 FoundLod = AvailableLods[SearchPosition];
			if (Lod < FoundLod)
			{
				MaxSearchPosition = SearchPosition;
			}
			else if (Lod > FoundLod)
			{
				MinSearchPosition = SearchPosition;
			}
			uint8 NewSearchPosition = ((MaxSearchPosition - MinSearchPosition) / 2) + MinSearchPosition;
			if (SearchPosition == NewSearchPosition)
			{
				return FoundLod;
			}
			SearchPosition = NewSearchPosition;
		}
	}
}

void UPolyVectorAsset::ClearMeshCache()
{
	this->MeshCache.Empty();
}
