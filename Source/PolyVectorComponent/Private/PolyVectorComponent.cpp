/**
 * Copyright ©2019 BattyBovine
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 **/


#include "PolyVectorComponent.h"

#include "PolyVectorAsset.h"
#include "ShapeBuilderTask.h"

#include "Async.h"
#include "ConstructorHelpers.h"
#include "TimerManager.h"
#include "UnrealEngine.h"
#include "Components/TimelineComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Misc/ConfigCacheIni.h"


#if !UE_BUILD_SHIPPING
DECLARE_CYCLE_STAT(TEXT("PolyVectorComponent::GetLodFromCameraDistance"), STAT_GetLodFromCameraDistance, STATGROUP_PolyVectorComponent);
DECLARE_CYCLE_STAT(TEXT("PolyVectorComponent::DrawCurrentFrame"), STAT_DrawCurrentFrame, STATGROUP_PolyVectorComponent);
#endif


UPolyVectorComponent::UPolyVectorComponent(const FObjectInitializer& ObjectInitializer) :
	bIsFrameDirty(true)
{
	ConstructorHelpers::FObjectFinder<UMaterial> VertexColourMaterial(TEXT("Material'/PolyVector/Materials/M_VertexColour.M_VertexColour'"));
	if (VertexColourMaterial.Succeeded())
	{
		this->DefaultMaterial = (UMaterial*)VertexColourMaterial.Object;
	}
	ConstructorHelpers::FObjectFinder<UMaterial> VertexColourMaskedMaterial(TEXT("Material'/PolyVector/Materials/M_VertexColourMasked.M_VertexColourMasked'"));
	if (VertexColourMaskedMaterial.Succeeded())
	{
		this->DefaultMaskedMaterial = (UMaterial*)VertexColourMaskedMaterial.Object;
	}

	// Load values from our settings file and store them locally
	if (GConfig)
	{
		GConfig->GetFloat(TEXT("/Script/PolyVectorComponent.PolyVectorGlobalSettings"), TEXT("LodEpsilon"), this->LodEpsilon, GGameIni);
		GConfig->GetFloat(TEXT("/Script/PolyVectorComponent.PolyVectorGlobalSettings"), TEXT("LayerDepth"), this->LayerDepth, GGameIni);
		GConfig->GetFloat(TEXT("/Script/PolyVectorComponent.PolyVectorGlobalSettings"), TEXT("LodDistanceScale"), this->LodDistanceScale, GGameIni);
		GConfig->GetFloat(TEXT("/Script/PolyVectorComponent.PolyVectorGlobalSettings"), TEXT("LodDistanceCurve"), this->LodDistanceCurve, GGameIni);
		GConfig->GetInt(TEXT("/Script/PolyVectorComponent.PolyVectorGlobalSettings"), TEXT("MaxLod"), this->MaxLod, GGameIni);
		GConfig->GetInt(TEXT("/Script/PolyVectorComponent.PolyVectorGlobalSettings"), TEXT("MaxShadowCasters"), this->MaxShadowCasters, GGameIni);
		GConfig->GetFloat(TEXT("/Script/PolyVectorComponent.PolyVectorGlobalSettings"), TEXT("MinAreaShadow"), this->MinAreaShadow, GGameIni);
	}
}

UPolyVectorComponent::~UPolyVectorComponent()
{
	if (this->ShapeBuilder)
	{
		this->ShapeBuilder->Cancel();
		this->ShapeBuilder->EnsureCompletion();
		delete this->ShapeBuilder;
	}
}

void UPolyVectorComponent::BeginPlay()
{
	Super::BeginPlay();

	// Set a timer for the LOD calculator
	this->GetWorld()->GetTimerManager().SetTimer(this->LodTimerHandle, this, &UPolyVectorComponent::GetLodFromCameraDistance, (1.0 / 30.0), true);
}


void UPolyVectorComponent::GetLodFromCameraDistance()
{
#if !UE_BUILD_SHIPPING
	SCOPE_CYCLE_COUNTER(STAT_GetLodFromCameraDistance);
#endif

	if (!this->PolyVectorAsset)
	{
		return;
	}

	const APlayerCameraManager* CameraManager = UGameplayStatics::GetPlayerCameraManager(this->GetWorld(), 0);
	if (CameraManager)
	{
		FVector CameraPosition = CameraManager->GetCameraLocation();
		const FVector PolyVectorPosition = this->GetOwner()->GetActorLocation();
		CameraPosition.Z = PolyVectorPosition.Z;

		const int8 NewLod = FMath::Clamp(FMath::RoundToInt(FMath::Pow((this->MaxLod - (FVector::Distance(CameraPosition, PolyVectorPosition) / this->LodDistanceScale)) * UPolyVectorComponent::GetLodScale(FVector2D(GSystemResolution.ResX, GSystemResolution.ResY)), this->LodDistanceCurve)), POLYVECTOR_MIN_QUALITY, this->MaxLod);

		if (this->CurrentLod != NewLod)
		{
			this->bIsFrameDirty = true;
			this->CurrentLod = NewLod;
		}
	}

	// If we don't have this LOD cached, begin caching it now
	if (!this->PolyVectorAsset->HasLod(this->CurrentLod))
	{
		// Don't do this if we are already caching something
		if (!this->ShapeBuilder)
		{
			this->ShapeBuilder = new FAsyncTask<FShapeBuilderTask>(this->PolyVectorAsset, this->CurrentLod - 1, this->CurrentLod + 1, this->LayerDepth, this->LayerDepthMultiplier, this->LodEpsilon);
			this->ShapeBuilder->GetTask().LodGroupComplete.BindUObject(this, &UPolyVectorComponent::LodCacheComplete);
			this->ShapeBuilder->GetTask().TaskComplete.BindUObject(this, &UPolyVectorComponent::ShapeBuildingComplete);
			this->ShapeBuilder->StartBackgroundTask();
		}
	}
}

void UPolyVectorComponent::DrawCurrentFrame(bool ForceDirty)
{
#if !UE_BUILD_SHIPPING
	SCOPE_CYCLE_COUNTER(STAT_DrawCurrentFrame);
#endif

	// If the asset file is null, do nothing
	if (!this->PolyVectorAsset)
	{
		this->bIsFrameDirty = false;
		return;
	}
	if (!ForceDirty &&	// Skip if we're not forcing a frame redraw
		!this->bIsFrameDirty &&	// Skip if the component has not requested a redraw
		!this->GetOwner()->WasRecentlyRendered())	// Skip if the owning actor has not recently been rendered to the screen
	{
		return;
	}
	this->bIsFrameDirty = false;

	this->MeshBuilder();
}


void UPolyVectorComponent::MeshBuilder()
{
	const uint16 Frame = this->CurrentFrame;
	const int8 Lod = this->PolyVectorAsset->GetNearestLod(this->CurrentLod);

	// Build a new display list if we need one
	const TArray<FPolyVectorSymbol>* SymbolsList = &this->PolyVectorAsset->Frames[Frame].Symbols;
	if (!this->DisplayListCache.Contains(Lod))
	{
		this->DisplayListCache.Emplace(Lod, UMeshInstanceFrames());
	}
	if(!this->DisplayListCache[Lod].Contains(Frame))
	{
		this->DisplayListCache[Lod].Emplace(Frame, NewObject<USceneComponent>(this, *FString::Printf(TEXT("Frame%5d"), Frame)));

		double LayerSeparation = 0.0;
		TMap<float, UPolyVectorMeshComponent*> ShadowCasterAreas;
		uint8 StencilMaskValue = 0;
		uint16 TranslucencySortOrder = 0;
		uint16 ClippingLayerMaxDepth = 0;
		for (const FPolyVectorSymbol& Symbol : *SymbolsList)
		{
			// Update the clipping layer depth if necessary
			if (ClippingLayerMaxDepth > 0 && Symbol.Depth > ClippingLayerMaxDepth && Symbol.ClipDepth == 0)
			{
				ClippingLayerMaxDepth = TranslucencySortOrder = 0;
			}

			// Render each shape layer as a separate component
			const FPolyVectorMeshSection MeshSection = this->PolyVectorAsset->GetCachedMesh(Lod, Symbol.ID);
			UPolyVectorMeshComponent* MeshComponent = NewObject<UPolyVectorMeshComponent>(this->DisplayListCache[Lod][Frame], NAME_None, EObjectFlags::RF_NoFlags);
			MeshComponent->SetRelativeLocation(FVector(LayerSeparation, Symbol.Transform.Translation.X, Symbol.Transform.Translation.Y));
			MeshComponent->SetArea(MeshSection.Area);
			MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			MeshComponent->bRenderCustomDepth = true;
			MeshComponent->SetCustomDepthStencilWriteMask(ERendererStencilMask::ERSM_128);
			MeshComponent->SetCustomDepthStencilValue(128);
			MeshComponent->SetProcMeshSection(0, MeshSection.Mesh);
			MeshComponent->RegisterComponent();
			MeshComponent->SetRelativeScale3D(FVector(1.0, Symbol.Transform.Scale.X, Symbol.Transform.Scale.Y));
			MeshComponent->SetRelativeSkew2D(Symbol.Transform.Skew);
#ifdef POLYVECTOR_MOVE_TO_HIDE
			MeshComponent->SetRelativeRotation(FRotator::ZeroRotator);
			MeshComponent->AttachToComponent(this->DisplayListCache[Lod][Frame], FAttachmentTransformRules::KeepRelativeTransform);
#else
			this->DisplayListCache[Lod][Frame].Emplace(Symbol.Depth, MeshComponent);
#endif

			// Set material if necessary
			UMaterialInterface* MaterialInterface = MeshComponent->GetMaterial(0);
			if (!MaterialInterface)
			{
				UMaterial* BaseMaterial = this->MaterialOverride ? this->MaterialOverride : this->DefaultMaterial;
				UMaterialInstanceDynamic* MaterialTransform = UMaterialInstanceDynamic::Create(BaseMaterial, this);
				MeshComponent->SetMaterial(0, MaterialTransform);
				MaterialInterface = MeshComponent->GetMaterial(0);
			}

			// Handle clipping layers through the material settings
			if (Symbol.ClipDepth > 0)
			{
				StencilMaskValue = (StencilMaskValue % 31) + 1;
				MeshComponent->SetRenderInMainPass(false);
				MeshComponent->SetCustomDepthStencilWriteMask(ERendererStencilMask::ERSM_255);
				MeshComponent->SetCustomDepthStencilValue(StencilMaskValue);
				ClippingLayerMaxDepth = Symbol.ClipDepth;
			}
			else
			{
				UMaterialInstanceDynamic* Material = nullptr;
				if (Symbol.Depth < ClippingLayerMaxDepth)
				{
					// Increment the translucency sort order for each masked symbol
					MeshComponent->TranslucencySortPriority = TranslucencySortOrder;
					TranslucencySortOrder++;

					// If this needs to be masked, set the mask value to our current clipping layer value
					UMaterial* MaskedMaterial = this->MaterialOverride ? this->MaterialOverride : this->DefaultMaskedMaterial;
					UMaterialInstanceDynamic* MaterialMaskedTransform = UMaterialInstanceDynamic::Create(MaskedMaterial, this);
					MeshComponent->SetMaterial(0, MaterialMaskedTransform);
					Material = Cast<UMaterialInstanceDynamic>(MeshComponent->GetMaterial(0));

					Material->SetScalarParameterValue("ClippingLayer", 1.0);
					Material->SetScalarParameterValue("MaskValue", 128+StencilMaskValue);
				}
				else
				{
					// Otherwise just get the current material and set the add/multiply values
					Material = Cast<UMaterialInstanceDynamic>(MaterialInterface);
				}
				Material->SetVectorParameterValue("Add", Symbol.ColourTransform.Add.ReinterpretAsLinear());
				Material->SetVectorParameterValue("Multiply", Symbol.ColourTransform.Multiply);
			}

			// Disable shadows, but add to list of potential shadow casters for checking later
			MeshComponent->SetCastShadow(false);
			ShadowCasterAreas.Emplace(MeshComponent->GetArea(), MeshComponent);

			// Push our layer separation value far enough away to compensate for the newly-placed symbol
			LayerSeparation += (this->LayerDepth * this->LayerDepthMultiplier) * MeshSection.Layers;
		}

		// Get our maximum number of largest shadow casters and set them to cast shadows
		TArray<float> ShadowCasterAreasKeys;
		ShadowCasterAreas.GetKeys(ShadowCasterAreasKeys);
		ShadowCasterAreasKeys.Sort([](const float& L, const float& R) { return L > R; });
		if (ShadowCasterAreasKeys.Num() > this->MaxShadowCasters)
		{
			ShadowCasterAreasKeys.SetNum(this->MaxShadowCasters, true);
		}
		for (float Key : ShadowCasterAreasKeys)
		{
			if (ShadowCasterAreas[Key]->GetArea() > this->MinAreaShadow)
			{
				ShadowCasterAreas[Key]->SetCastShadow(true);
			}
		}
	}

	// Make all previous symbols invisible
	if (this->CurrentDisplayList)
	{
#ifdef POLYVECTOR_MOVE_TO_HIDE
		CurrentDisplayList->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
		CurrentDisplayList->SetWorldLocation(FVector(0.0, 0.0, -10000.0));
#else
		for (TPair<const uint16, UPolyVectorMeshComponent*>& Component : *this->CurrentDisplayList)
		{
			Component.Value->SetVisibility(false);
		}
#endif
	}

	// Make the currently needed symbols visible
	this->CurrentDisplayList = this->DisplayListCache[Lod][Frame];
#ifdef POLYVECTOR_MOVE_TO_HIDE
	if (this->CurrentDisplayList)
	{
		this->CurrentDisplayList->AttachToComponent(this, FAttachmentTransformRules::KeepRelativeTransform);
		this->CurrentDisplayList->SetRelativeLocation(FVector::ZeroVector);
		this->CurrentDisplayList->SetRelativeRotation(FRotator::ZeroRotator);
		this->CurrentDisplayList->SetRelativeScale3D(FVector::OneVector);
	}
#else
	double LayerSeparation = 0.0;
	for (const FPolyVectorSymbol& Symbol : *SymbolsList)
	{
		UPolyVectorMeshComponent* Component = (*this->CurrentDisplayList)[Symbol.Depth];
		Component->AttachToComponent(this, FAttachmentTransformRules::KeepRelativeTransform, NAME_None);
		Component->SetRelativeLocation(FVector(LayerSeparation, Symbol.Transform.Translation.X, Symbol.Transform.Translation.Y));
		Component->SetRelativeRotation(FRotator::ZeroRotator);
		(*this->CurrentDisplayList)[Symbol.Depth]->SetVisibility(true);
		const FPolyVectorMeshSection MeshSection = this->PolyVectorAsset->GetCachedMesh(Lod, Symbol.ID);
		LayerSeparation += (this->LayerDepth * this->LayerDepthMultiplier) * MeshSection.Layers;
	}
#endif
}


void UPolyVectorComponent::SetPolyVectorAsset(UPolyVectorAsset* NewPolyVectorAsset)
{
	if (this->ShapeBuilder)
	{
		// Cancel the shape builder task if it's running, and exit early if we can't for some reason
		if (this->ShapeBuilder->Cancel() || this->ShapeBuilder->IsDone())
		{
			delete this->ShapeBuilder;
			this->ShapeBuilder = nullptr;
		}
		else
		{
			return;
		}
	}

	this->PolyVectorAsset = NewPolyVectorAsset;

	// Clear mesh cache and children in preparation for the new asset
	this->ClearMeshComponents();

	// If our new asset happens to be null, we can stop here
	if (!this->PolyVectorAsset)
	{
		return;
	}

	const uint16 EndFrame = this->PolyVectorAsset->Frames.Num();
	if (this->PolyVectorAsset->SceneLabelData.Num() <= 0 && this->PolyVectorAsset->FrameLabelData.Num() <= 0)
	{
		// Create a default animation that consists of the entire timeline if no scene or frame labels exist
		FPolyVectorAnimationData AnimationData;
		AnimationData.EndFrame = EndFrame;
		this->Animations.Add(NAME_None, AnimationData);
	}
	else
	{
		// Otherwise, create definitions for each labelled animation
		for (TMap<FName, uint16>::TConstIterator FrameLabelIterator = this->PolyVectorAsset->FrameLabelData.CreateConstIterator(); FrameLabelIterator; ++FrameLabelIterator)
		{
			FPolyVectorAnimationData AnimationData;
			AnimationData.StartFrame = (*FrameLabelIterator).Value;
			TMap<FName, uint16>::TConstIterator NextFrameLabelIterator = FrameLabelIterator;
			AnimationData.EndFrame = (++NextFrameLabelIterator) ? (*NextFrameLabelIterator).Value : EndFrame;
			this->Animations.Add((*FrameLabelIterator).Key, AnimationData);
		}
	}

	// Set animation to the first by default
	this->SetNewAnimation((*this->Animations.begin()).Key);

	// Preload LOD 0 so that we at least have something to show while the rest loads
	// We also need to check whether we are in the world, or else it crashes during
	// packaging, but not on uncooked builds for some reason? It's weird and stupid.
	if (this->GetWorld())
	{
		this->ShapeBuilder = new FAsyncTask<FShapeBuilderTask>(this->PolyVectorAsset, 0, 0, this->LayerDepth, this->LayerDepthMultiplier, this->LodEpsilon);
		this->ShapeBuilder->GetTask().LodGroupComplete.BindUObject(this, &UPolyVectorComponent::LodCacheComplete);
		this->ShapeBuilder->GetTask().TaskComplete.BindUObject(this, &UPolyVectorComponent::ShapeBuildingComplete);
		this->ShapeBuilder->StartSynchronousTask();
		this->ShapeBuilder->EnsureCompletion();
		delete this->ShapeBuilder;
		this->ShapeBuilder = nullptr;
	}
}

void UPolyVectorComponent::SetNewAnimation(const FName& Animation)
{
	this->CurrentAnimation = Animation;

	// Reset the animation timer if necessary
	if (this->PolyVectorAsset && this->GetWorld() && this->GetWorld()->GetTimerManager().IsTimerActive(this->AnimationTimerHandle))
	{
		this->UpdateAnimation();
		this->GetWorld()->GetTimerManager().SetTimer(this->AnimationTimerHandle, this, &UPolyVectorComponent::UpdateAnimation, 1.0 / this->PolyVectorAsset->FPS * this->TimeDilation, true);
	}
}

void UPolyVectorComponent::UpdateAnimation()
{
	if (this->Animations.Num() > 0)
	{
		const FPolyVectorAnimationData& Animation = this->Animations[this->CurrentAnimation];
		this->CurrentFrame = ((this->CurrentFrame + 1) % (Animation.EndFrame - Animation.StartFrame)) + Animation.StartFrame;

		this->DrawCurrentFrame(true);
	}
}


void UPolyVectorComponent::ClearMeshComponents()
{
	// Remove all PolyVectorMeshComponent children and clear the display list cache
	for (TPair<const int8, UMeshInstanceFrames> MeshInstanceFrame : this->DisplayListCache)
	{
#ifdef POLYVECTOR_MOVE_TO_HIDE
		for (TPair<const uint16, USceneComponent*> MeshInstanceLayers : MeshInstanceFrame.Value)
		{
			MeshInstanceLayers.Value->DestroyComponent();
		}
#else
		for (TPair<const uint16, UMeshInstanceLayers> MeshInstanceLayer : MeshInstanceFrame.Value)
		{
			for (TPair<const uint16, UPolyVectorMeshComponent*> MeshComponent : MeshInstanceLayer.Value)
			{
				if (MeshComponent.Value)
				{
					MeshComponent.Value->DestroyComponent();
				}
			}
		}
#endif
	}
	this->DisplayListCache.Empty();

	this->CurrentDisplayList = nullptr;
}


void UPolyVectorComponent::GetGeneratedMesh(UPolyVectorMeshComponent* MeshComponent, FProcMeshSection Mesh)
{
	MeshComponent->SetProcMeshSection(0, Mesh);
}

void UPolyVectorComponent::LodCacheComplete(FPolyVectorMeshes MeshCache, int8 Lod)
{
	this->PolyVectorAsset->SetCachedMeshes(MeshCache, Lod);
	if (Lod == this->CurrentLod && this->PolyVectorAsset && this->GetWorld())
	{
		this->GetWorld()->GetTimerManager().SetTimer(this->AnimationTimerHandle, this, &UPolyVectorComponent::UpdateAnimation, 1.0 / this->PolyVectorAsset->FPS * this->TimeDilation, true);
	}
}

void UPolyVectorComponent::ShapeBuildingComplete()
{
	if (this->ShapeBuilder)
	{
		this->ShapeBuilder->EnsureCompletion();
		delete this->ShapeBuilder;
		this->ShapeBuilder = nullptr;
	}

	if (this->PolyVectorAsset && this->GetWorld() && !this->GetWorld()->GetTimerManager().IsTimerActive(this->AnimationTimerHandle))
	{
		this->GetWorld()->GetTimerManager().SetTimer(this->AnimationTimerHandle, this, &UPolyVectorComponent::UpdateAnimation, 1.0 / this->PolyVectorAsset->FPS * this->TimeDilation, true);
	}
}
