/**
 * Copyright �2019 BattyBovine
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 **/


#include "ShapeBuilderTask.h"

#include <vector>

#include "Async.h"
#include "PolyVectorAsset.h"


void FShapeBuilderTask::DoWork()
{
	if (this->PolyVectorAsset == nullptr)
	{
		return;
	}

	float LayerSeparation = 0.0;
	const uint16 FrameCount = this->PolyVectorAsset->Frames.Num();

	for (int8 Lod = this->MinimumLod; Lod <= MaximumLod; Lod++)
	{
		FPolyVectorMeshes MeshCache;
		for (uint16 Frame = 0; Frame < FrameCount; Frame++)
		{
			const TArray<FPolyVectorSymbol>& Symbols = this->PolyVectorAsset->Frames[Frame].Symbols;
			for (const FPolyVectorSymbol& Symbol : Symbols)
			{
				const FPolyVectorShapeCharacter& PolyVectorCharacter = this->PolyVectorAsset->Library.Characters[Symbol.ID];
				FPolyVectorMeshSection MeshSection;
				MeshSection.ID = Symbol.ID;
				MeshSection.Mesh.bEnableCollision = false;
				MeshSection.Mesh.bSectionVisible = true;
				float LargestShapeArea = 0.0;
				const FVector Normal(1.0, 0.0, 0.0);					// Should never change; all polygons are on the same plane
				const FProcMeshTangent& Tangent = FProcMeshTangent();	// Same here
				uint32 IndexCounter = 0;
				for (const FPolyVectorShape& Shape : PolyVectorCharacter.Shapes)
				{
					// Skip if this shape has no fill; it's a hole
					if (Shape.Fill == 0)
					{
						continue;
					}

					// Find the largest shape area in our character mesh
					if (Shape.Area > LargestShapeArea)
					{
						LargestShapeArea = Shape.Area;
					}

					std::vector< std::vector<FVector2D> > Polygons;
					TArray<FVector2D> TessellatedCurve;
					if (Shape.Path.Curve.GetPointCount() <= 3)
					{
						for (uint32 i = 0; i < Shape.Path.Curve.GetPointCount(); i++)
						{
							TessellatedCurve.Add(Shape.Path.Curve.GetPointPosition(i));
						}
					}
					else if (Lod >= 0)
					{
						TessellatedCurve = Shape.Path.Curve.Tessellate(Lod);
					}
					else
					{
						//TessellatedCurve = this->VwSimplify(Shape.Path.Curve.Tessellate(0), FMath::Abs(this->CurrentLod));
						TessellatedCurve = this->RdpSimplify(Shape.Path.Curve.Tessellate(0), FMath::Abs(Lod) * this->LodEpsilon);
					}
					//if(!shape.path.closed) {	// If shape is not a closed loop, store as a stroke
					//	shape.strokes[this->iCurveQuality].push_back(tess);
					//} else {					// Otherwise, triangulate
					const uint32 PointCount = TessellatedCurve.Num();
					std::vector<FVector2D> Poly;
					for (uint32 i = 1; i < PointCount; i++)	// Push vertices into a standard vector
					{
						Poly.push_back(TessellatedCurve[i]);
					}
					Polygons.push_back(Poly);
					std::vector<FVector2D> TessellatedVertices;
					TessellatedVertices.insert(TessellatedVertices.end(), Poly.begin(), Poly.end());
					for (uint16 Hole : Shape.Holes)
					{
						TArray<FVector2D> TessellatedHole = PolyVectorCharacter.Shapes[Hole].Path.Curve.Tessellate(FMath::Clamp(Lod, (int8)0, (int8)INT8_MAX));
						const uint32 TessellatedHoleCount = TessellatedHole.Num();
						std::vector<FVector2D> HolePolygon;
						for (uint32 j = 0; j < TessellatedHoleCount; j++)
						{
							HolePolygon.push_back(TessellatedHole[j]);
						}
						Polygons.push_back(HolePolygon);
						TessellatedVertices.insert(TessellatedVertices.end(), HolePolygon.begin(), HolePolygon.end());
					}
					//}
					if (!Polygons.empty())
					{
						std::vector<N> EarIndices = mapbox::earcut<N>(Polygons);
						if (EarIndices.empty())
						{
							continue;
						}
						const FColor Colour = this->PolyVectorAsset->Library.FillStyles[Symbol.ID].Array[Shape.Fill - 1].Colour;
						const float XPosition = (this->LayerDepth * this->LayerDepthMultiplier) * MeshSection.Layers;
						// Add vertices in reverse order so the normals are facing forward
						for (std::vector<N>::reverse_iterator Idx = EarIndices.rbegin(); Idx != EarIndices.rend(); Idx++)
						{
							FProcMeshVertex Vertex;
							MeshSection.Mesh.ProcIndexBuffer.Add(IndexCounter++);
							Vertex.Color = Colour;
							Vertex.Normal = Normal;
							const FVector2D Vert = TessellatedVertices[*Idx];
							Vertex.Position = FVector(XPosition, Vert.X, Vert.Y);
							Vertex.UV0 = FVector2D(
								FMath::Frac(FMath::GetMappedRangeValueClamped(FVector2D(PolyVectorCharacter.Bounds.Min.X, PolyVectorCharacter.Bounds.Max.X), FVector2D(0.0, 1.0), Vert.X)),
								FMath::Frac(FMath::GetMappedRangeValueClamped(FVector2D(PolyVectorCharacter.Bounds.Min.Y, PolyVectorCharacter.Bounds.Max.Y), FVector2D(0.0, 1.0), Vert.Y))
							);
							Vertex.Tangent = Tangent;
							MeshSection.Mesh.ProcVertexBuffer.Add(Vertex);
							MeshSection.Mesh.SectionLocalBox += Vertex.Position;
						}
						MeshSection.Layers++;
					}
				}
				MeshSection.Area = LargestShapeArea;
				MeshCache.Emplace(Symbol.ID, MeshSection);
			}
		}

		const FShapeBuilderLodGroupDelegate& GroupComplete = this->LodGroupComplete;
		AsyncTask(ENamedThreads::GameThread, [GroupComplete, MeshCache, Lod]() {
			GroupComplete.Execute(MeshCache, Lod);
		});
	}

	const FShapeBuilderCompleteDelegate& Complete = this->TaskComplete;
	AsyncTask(ENamedThreads::GameThread, [Complete]() {
		Complete.Execute();
	});
}


TArray<FVector2D> FShapeBuilderTask::VwSimplify(const TArray<FVector2D> &PointList, uint8 Factor)
{
	TArray<FVector2D> PointsOut;
	PointsOut.Append(PointList);
	const uint32 PointCount = PointList.Num();

	uint32 KillCount = PointCount - FMath::Clamp<uint32>(PointCount / (Factor + 1), 3, PointCount);
	while (KillCount-- > 0)
	{
		uint32 Index = 1;
		double MinimumArea = this->AreaOfTriangle(PointsOut[0], PointsOut[1], PointsOut[2]);
		const uint32 PointsOutNum = (PointsOut.Num() - 2);
		for (uint32 i = 2; i < PointsOutNum; i++)
		{
			double Area = this->AreaOfTriangle(PointsOut[i - 1], PointsOut[i], PointsOut[i + 1]);
			if (Area < MinimumArea)
			{
				MinimumArea = Area;
				Index = i;
			}
		}
		PointsOut.RemoveAt(Index);
	}

	return PointsOut;
}
double FShapeBuilderTask::AreaOfTriangle(FVector2D A, FVector2D B, FVector2D C)
{
	return FMath::Abs((A.X * (B.Y - C.Y)) + (B.X * (C.Y - A.Y)) + (C.X * (A.Y - B.Y))) / 2.0;
}

TArray<FVector2D> FShapeBuilderTask::RdpSimplify(const TArray<FVector2D> &PointList, double Epsilon)
{
	if (PointList.Num() < 3)
	{
		return PointList;
	}

	TArray<FVector2D> Out;

	// Find the point with the maximum distance from line between start and end
	double DMax = 0.0;
	uint32 Index = 0;
	const uint32 End = PointList.Num() - 1;
	for (uint32 i = 1; i < End; i++)
	{
		double D = this->PerpendicularDistance(PointList[i], PointList[0], PointList[End]);
		if (D > DMax)
		{
			Index = i;
			DMax = D;
		}
	}

	// If max distance is greater than epsilon, recursively simplify
	if (DMax > Epsilon)
	{
		TArray<FVector2D> FirstLine, LastLine;
		for (uint32 i = 0; i < Index + 1; i++)
		{
			FirstLine.Add(PointList[i]);
		}
		const uint32 PointListSize = PointList.Num();
		for (uint32 i = Index; i < PointListSize; i++)
		{
			LastLine.Add(PointList[i]);
		}

		TArray<FVector2D> R1 = this->RdpSimplify(FirstLine, Epsilon);
		TArray<FVector2D> R2 = this->RdpSimplify(LastLine, Epsilon);

		Out = R1;
		Out.Append(R2);
		if (Out.Num() < 3)
		{
			return PointList;
		}
	}
	else
	{
		Out.Add(PointList[0]);
		Out.Add(PointList[End]);
	}

	return Out;
}

double FShapeBuilderTask::PerpendicularDistance(const FVector2D& Point, const FVector2D& Start, const FVector2D& End)
{
	FVector2D D(End.X - Start.X, End.Y - Start.Y);
	double Magnitude = FMath::Pow(FMath::Pow(D.X, 2.0) + FMath::Pow(D.Y, 2.0), 0.5);
	if (Magnitude > 0.0)
	{
		D /= Magnitude;
	}
	FVector2D PV(Point.X - Start.X, Point.Y - Start.Y);
	double PVDot = FVector2D::DotProduct(D, PV);
	FVector2D A = PV - FVector2D(PVDot * D.X, PVDot * D.Y);
	return FMath::Pow(FMath::Pow(A.X, 2.0) + FMath::Pow(A.Y, 2.0), 0.5);
}
