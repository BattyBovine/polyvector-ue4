/**
 * Copyright �2019 BattyBovine
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 **/


#pragma once

#include "CoreMinimal.h"

#include "AsyncWork.h"
#include "earcut.hpp/earcut.hpp"


using N = uint32;
using Coord = float;
using Point = FVector2D;
namespace mapbox {
	namespace util {
		template <> struct nth<0, FVector2D> { inline static auto get(const FVector2D &V) { return V.X; }; };
		template <> struct nth<1, FVector2D> { inline static auto get(const FVector2D &V) { return V.Y; }; };
	}
}


class FShapeBuilderTask : public FNonAbandonableTask
{
	friend class FAsyncTask<FShapeBuilderTask>;
	
public:	
	// Sets default values for this actor's properties
	FShapeBuilderTask(class UPolyVectorAsset* Asset, int8 MinLod, int8 MaxLod, float Depth, float Multiplier, float Epsilon) :
		PolyVectorAsset(Asset),
		MinimumLod(MinLod),
		MaximumLod(MaxLod),
		LayerDepth(Depth),
		LayerDepthMultiplier(Multiplier),
		LodEpsilon(Epsilon)
	{}

	DECLARE_DELEGATE_TwoParams(FShapeBuilderLodGroupDelegate, FPolyVectorMeshes, int8);
	FShapeBuilderLodGroupDelegate LodGroupComplete;

	DECLARE_DELEGATE(FShapeBuilderCompleteDelegate);
	FShapeBuilderCompleteDelegate TaskComplete;

protected:
	// Called when the game starts or when spawned
	void DoWork();

	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(FShapeBuilderTask, STATGROUP_ThreadPoolAsyncTasks);
	}

private:
	TArray<FVector2D> VwSimplify(const TArray<FVector2D> &PointList, uint8 Factor);
	FORCEINLINE double AreaOfTriangle(FVector2D A, FVector2D B, FVector2D C);
	TArray<FVector2D> RdpSimplify(const TArray<FVector2D> &PointList, double Epsilon);
	FORCEINLINE double PerpendicularDistance(const FVector2D& Point, const FVector2D& Start, const FVector2D& End);

	class UPolyVectorAsset* PolyVectorAsset = nullptr;
	const int8 MinimumLod = 0;
	const int8 MaximumLod = 0;
	const float LayerDepth = 0.0;
	const float LayerDepthMultiplier = 1.0;
	const float LodEpsilon = 0.0;
};
