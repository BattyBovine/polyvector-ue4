#pragma once

#include "CoreMinimal.h"

#include "Curve2D.generated.h"

/**
 * 2D Bezier curve which can be tessellated as needed
 */
USTRUCT()
struct FBezierPoint2D
{
	GENERATED_BODY()
	UPROPERTY()	FVector2D Position;
	UPROPERTY()	FVector2D In;
	UPROPERTY()	FVector2D Out;
};

USTRUCT()
struct POLYVECTORCOMPONENT_API FCurve2D
{
	GENERATED_BODY()

public:
	uint32 GetPointCount() const;
	void AddPoint(const FVector2D &Pos, const FVector2D &In = FVector2D(), const FVector2D &Out = FVector2D(), int32 AtPos = -1);
	FORCEINLINE FVector2D GetPointPosition(uint32 Index) const { return this->Points[Index].Position; }
	TArray<FVector2D> GetData() const;
	TArray<FVector2D> GetPositionData() const;

	TArray<FVector2D> Tessellate(const uint32 PointCount = 0) const;

private:
	UPROPERTY()	TArray<FBezierPoint2D> Points;
};

DECLARE_LOG_CATEGORY_EXTERN(LogCurve2D, Warning, All);
