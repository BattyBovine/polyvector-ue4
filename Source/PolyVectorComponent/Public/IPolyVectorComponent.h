/**
 * Copyright ©2019 BattyBovine
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 **/

#pragma once

#include "CoreMinimal.h"
//#include "UnrealNames.h"
#include "Modules/ModuleInterface.h"
#include "Modules/ModuleManager.h"


/**
 * The public interface of the IPolyVectorComponent module
 */
class IPolyVectorComponent : public IModuleInterface
{
public:
	/**
	 * Singleton-like access to IPolyVectorComponent
	 *
	 * @return Returns IPolyVectorComponent singleton instance, loading the module on demand if needed
	 */
	static IPolyVectorComponent& Get()
	{
		return FModuleManager::LoadModuleChecked<IPolyVectorComponent>("IPolyVectorComponent");
	}

	/**
	 * Checks to see if this module is loaded and ready.  It is only valid to call Get() if IsAvailable() returns true.
	 *
	 * @return True if the module is loaded and ready to use
	 */
	static bool IsAvailable()
	{
		// static const FName ModuleName("IPolyVectorComponent");
		return FModuleManager::Get().IsModuleLoaded("IPolyVectorComponent");
	}
};


DECLARE_LOG_CATEGORY_EXTERN(LogPolyVector, Log, All);
