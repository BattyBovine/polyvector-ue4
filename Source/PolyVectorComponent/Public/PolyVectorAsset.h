/**
 * Copyright ©2019 BattyBovine
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 **/


#pragma once

#include "CoreMinimal.h"
#include "Curve2D.h"

#include "ProceduralMeshComponent.h"

#include "PolyVectorAsset.generated.h"


UENUM()
enum class EPolyVectorFillType : uint8
{
	None,
	Solid,
	Gradient
};


USTRUCT()
struct POLYVECTORCOMPONENT_API FPolyVectorFillStyle
{
	GENERATED_BODY()
	FPolyVectorFillStyle(uint8 R = 255, uint8 G = 0, uint8 B = 255, uint8 A = 255)
	{
		this->FillType = EPolyVectorFillType::Solid;
		this->Colour = FColor(R, G, B, A);
	}
	UPROPERTY()	EPolyVectorFillType FillType;
	UPROPERTY()	FColor				Colour;
};
USTRUCT() // Necessary to support nested arrays
struct POLYVECTORCOMPONENT_API FPolyVectorFillStyles
{
	GENERATED_BODY()
	UPROPERTY()	TArray<FPolyVectorFillStyle> Array;
};

USTRUCT()
struct POLYVECTORCOMPONENT_API FPolyVectorPath
{
	GENERATED_BODY()
	UPROPERTY()	uint8		Closed : 1;
	UPROPERTY()	FCurve2D	Curve;
};

USTRUCT()
struct POLYVECTORCOMPONENT_API FPolyVectorShape
{
	GENERATED_BODY()
	UPROPERTY()	FPolyVectorPath	Path;
	UPROPERTY()	uint16			Fill = 0;
	UPROPERTY()	TArray<uint16>	Holes;
	UPROPERTY()	float			Area = 0.0;
};

USTRUCT()
struct POLYVECTORCOMPONENT_API FPolyVectorBoundsRect
{
	GENERATED_BODY()
	UPROPERTY()	FVector2D	Min;
	UPROPERTY()	FVector2D	Max;
};

USTRUCT()
struct POLYVECTORCOMPONENT_API FPolyVectorShapeCharacter
{
	GENERATED_BODY()
	UPROPERTY()	FPolyVectorBoundsRect		Bounds;
	UPROPERTY() TArray<FPolyVectorShape>	Shapes;
};

USTRUCT()
struct POLYVECTORCOMPONENT_API FPolyVectorLibrary
{
	GENERATED_BODY()
	UPROPERTY()	TArray<FPolyVectorFillStyles>		FillStyles;
	UPROPERTY()	TArray<FPolyVectorShapeCharacter>	Characters;
};

USTRUCT()
struct POLYVECTORCOMPONENT_API FPolyVectorTransform
{
	GENERATED_BODY()
	UPROPERTY()	FVector2D	Translation;
	UPROPERTY()	FVector2D	Scale;
	UPROPERTY()	FVector2D	Skew;
};

USTRUCT()
struct POLYVECTORCOMPONENT_API FPolyVectorColourTransform
{
	GENERATED_BODY()
	UPROPERTY()	FColor			Add = FColor::Black;
	UPROPERTY()	FLinearColor	Multiply = FLinearColor::White;
	FORCEINLINE bool IsChanged() { return !(Add == FColor::Black || Multiply == FLinearColor::White); }
};

USTRUCT()
struct POLYVECTORCOMPONENT_API FPolyVectorSymbol
{
	GENERATED_BODY()
	UPROPERTY()	int32						ID = 0;
	UPROPERTY()	int32						Depth = 0;
	UPROPERTY()	FPolyVectorTransform		Transform;
	UPROPERTY()	FPolyVectorColourTransform	ColourTransform;
	UPROPERTY()	uint16						ClipDepth = 0;

};

USTRUCT()
struct POLYVECTORCOMPONENT_API FPolyVectorFrameDisplayList
{
	GENERATED_BODY()
	UPROPERTY()	TArray<FPolyVectorSymbol>	Symbols;
};

USTRUCT()
struct POLYVECTORCOMPONENT_API FPolyVectorFrameList
{
	GENERATED_BODY()

	FORCEINLINE FPolyVectorFrameDisplayList& operator[](uint32 Index) { return this->DisplayList[Index]; }
	FORCEINLINE uint32 Num() const { return this->DisplayList.Num(); }
	FORCEINLINE void Push(FPolyVectorFrameDisplayList& NewDisplayList) { this->DisplayList.Push(NewDisplayList); }
	FORCEINLINE void Empty(int32 Slack=0) { this->DisplayList.Empty(Slack); }

	UPROPERTY()	TArray<FPolyVectorFrameDisplayList>	DisplayList;
};


struct FPolyVectorMeshSection
{
	uint16 ID = 0;
	uint16 Layers = 0;
	float Area = 0.0;
	FProcMeshSection Mesh;
};
typedef TMap<const uint16, const FPolyVectorMeshSection> FPolyVectorMeshes;
typedef TMap<int8, FPolyVectorMeshes> FPolyVectorMeshLibrary;


UCLASS(BlueprintType, hidecategories=(Object))
class POLYVECTORCOMPONENT_API UPolyVectorAsset : public UObject
{
	GENERATED_BODY()

public:
	void SetCachedMesh(const FPolyVectorMeshSection& MeshSection, const int8 Lod, const uint16 ID);
	void SetCachedMeshes(const FPolyVectorMeshes& Meshes, const int8 Lod);
	FPolyVectorMeshSection GetCachedMesh(const int8 Lod, const uint16 ID) const;
	bool HasCachedMesh(const int8 Lod, const uint16 ID) const;
	bool HasLod(const int8 Lod) const;
	int8 GetNearestLod(const int8 Lod) const;
	void ClearMeshCache();

	UPROPERTY()	float					FPS = 0.0;
	UPROPERTY()	FVector2D				Dimensions;
	UPROPERTY()	FPolyVectorLibrary		Library;
	UPROPERTY()	FPolyVectorFrameList	Frames;
	UPROPERTY() TMap<FName, uint16>		SceneLabelData;
	UPROPERTY() TMap<FName, uint16>		FrameLabelData;

#if WITH_EDITORONLY_DATA
	UPROPERTY(VisibleAnywhere, Instanced, Category = ImportSettings)
		class UAssetImportData* AssetImportData;
#endif

private:
	FPolyVectorMeshLibrary MeshCache;
};
