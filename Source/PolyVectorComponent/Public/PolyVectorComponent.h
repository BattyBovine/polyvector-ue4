/**
 * Copyright ©2019 BattyBovine
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 **/


#pragma once

#include "CoreMinimal.h"
#include "ProceduralMeshComponent.h"

#include "PolyVectorAsset.h"

#include "PolyVectorComponent.generated.h"


#define POLYVECTOR_MIN_QUALITY	-10

// Optimisation options
#define POLYVECTOR_MOVE_TO_HIDE
// End optimisation options


USTRUCT()
struct FPolyVectorAnimationData
{
	GENERATED_BODY()

	uint16 StartFrame = 0;
	uint16 EndFrame = 0;
};
typedef TMap<const FName, const FPolyVectorAnimationData> FAnimationsMap;

UCLASS(ClassGroup=Custom)
class POLYVECTORCOMPONENT_API UPolyVectorMeshComponent : public UProceduralMeshComponent
{
	GENERATED_BODY()

public:
	void SetID(const uint16 NewID)
	{
		this->ID = NewID;
	}
	uint16 GetID() const
	{
		return this->ID;
	}
	void SetArea(const float NewArea)
	{
		this->Area = NewArea;
	}
	uint16 GetArea() const
	{
		return this->Area;
	}
	void SetRelativeSkew2D(const FVector2D& Skew)
	{
		this->LocalSkew = Skew;
	}
	virtual FMatrix GetRenderMatrix() const override
	{
		FMatrix NewMatrix = Super::GetRenderMatrix();
		//NewMatrix.M[1][2] = this->LocalSkew.X;
		//NewMatrix.M[2][1] = this->LocalSkew.Y;
		return NewMatrix;
	}

private:
	UPROPERTY()	uint16 ID = 0;
	UPROPERTY()	float Area = 0.0;

	FVector2D LocalSkew;
};
#ifdef POLYVECTOR_MOVE_TO_HIDE
typedef TMap<const uint16, USceneComponent*> UMeshInstanceFrames;
#else
typedef TMap<const uint16, UPolyVectorMeshComponent*> UMeshInstanceLayers;
typedef TMap<const uint16, UMeshInstanceLayers> UMeshInstanceFrames;
#endif

typedef TMap<const uint64, UMaterialInstanceDynamic*> UPolyVectorMaterialCache;


UCLASS( Blueprintable, ClassGroup=(Rendering), meta=(BlueprintSpawnableComponent) )
class POLYVECTORCOMPONENT_API UPolyVectorComponent : public USceneComponent
{
	GENERATED_BODY()

public:	
	UPolyVectorComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual ~UPolyVectorComponent();
	virtual void BeginPlay() override;

	void SetNewAnimation(const FName& Animation);

	UFUNCTION(BlueprintSetter)	void SetPolyVectorAsset(class UPolyVectorAsset* NewPolyVectorAsset);
	UFUNCTION(BlueprintGetter)	class UPolyVectorAsset* GetPolyVectorAsset() const { return this->PolyVectorAsset; }

	UFUNCTION(BlueprintSetter)	void SetLayerDepthMultiplier(const float NewMultiplier) { this->LayerDepthMultiplier = NewMultiplier; }
	UFUNCTION(BlueprintGetter)	float GetLayerDepthMultiplier() const { return this->LayerDepthMultiplier; }

	UFUNCTION(BlueprintSetter)	void SetMaterialOverride(class UMaterial* NewMaterial) { this->MaterialOverride = NewMaterial; }
	UFUNCTION(BlueprintGetter)	class UMaterial* GetMaterialOverride() const { return this->MaterialOverride; }

	UFUNCTION(BlueprintPure)	float GetLodDistanceScale() const { return this->LodDistanceScale; }
	UFUNCTION(BlueprintPure)	float GetLodDistanceCurve() const { return this->LodDistanceCurve; }
	UFUNCTION(BlueprintPure)	float GetLodEpsilon() const { return this->LodEpsilon; }
	UFUNCTION(BlueprintPure)	float GetLayerDepth() const { return this->LayerDepth; }
	UFUNCTION(BlueprintPure)	int32 GetMaxLod() const { return this->MaxLod; }

	// Static function that lets us calculate the current LOD scale based on the resolution, using 1080p as the baseline
	static float GetLodScale(const FVector2D& Resolution) { return ((1920.0 / Resolution.X) + (1080.0 / Resolution.Y)) / 2.0; }

protected:
	UPROPERTY(EditAnywhere, Category="PolyVector", BlueprintSetter="SetPolyVectorAsset", BlueprintGetter="GetPolyVectorAsset")
		class UPolyVectorAsset* PolyVectorAsset = nullptr;

	UPROPERTY(EditAnywhere, Category="PolyVector", BlueprintSetter="SetLayerDepthMultiplier", BlueprintGetter="GetLayerDepthMultiplier")
		float LayerDepthMultiplier = 1.0;

	UPROPERTY(EditAnywhere, Category="Materials", BlueprintSetter="SetMaterialOverride", BlueprintGetter="GetMaterialOverride")
		class UMaterial* MaterialOverride = nullptr;

private:
	void GetLodFromCameraDistance();
	void DrawCurrentFrame(bool ForceDirty=false);
	void UpdateAnimation();

	void MeshBuilder();
	void ClearMeshComponents();

	void GetGeneratedMesh(UPolyVectorMeshComponent* MeshComponent, FProcMeshSection Mesh);

	void LodCacheComplete(FPolyVectorMeshes MeshCache, int8 Lod);
	void ShapeBuildingComplete();

#ifdef POLYVECTOR_MOVE_TO_HIDE
	USceneComponent* CurrentDisplayList = nullptr;
#else
	UMeshInstanceLayers* CurrentDisplayList = nullptr;
#endif
	TMap<const int8, UMeshInstanceFrames> DisplayListCache;
	class FAsyncTask<class FShapeBuilderTask>* ShapeBuilder = nullptr;

	UMaterial* DefaultMaterial = nullptr;
	UMaterial* DefaultMaskedMaterial = nullptr;
	UPolyVectorMaterialCache MaterialCache;

	int8 CurrentLod = 0;
	uint16 CurrentFrame = 0;
	uint8 bIsFrameDirty : 1;

	FAnimationsMap Animations;
	FName CurrentAnimation = NAME_None;
	float Time = 0.0;
	float TimeDilation = 1.0;
	struct FTimerHandle AnimationTimerHandle;

	float LodDistanceScale = 350.0;
	float LodDistanceCurve = 0.75;
	float LodEpsilon = 0.025;
	float LayerDepth = 0.002;
	int32 MaxLod = 4;
	int32 MaxShadowCasters = UINT16_MAX;
	float MinAreaShadow = 0.0;
	struct FTimerHandle LodTimerHandle;
};


DECLARE_STATS_GROUP(TEXT("PolyVectorComponent"), STATGROUP_PolyVectorComponent, STATCAT_Advanced);
