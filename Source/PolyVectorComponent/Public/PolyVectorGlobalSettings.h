/**
* Copyright ©2019 BattyBovine
*
* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
**/

#pragma once

#include "Engine/DeveloperSettings.h"
#include "PolyVectorGlobalSettings.generated.h"


/**
 * Global settings for PolyVector components
 */
UCLASS(Config=Game, defaultconfig, meta = (DisplayName = "PolyVector"))
class POLYVECTORCOMPONENT_API UPolyVectorGlobalSettings : public UDeveloperSettings
{
	GENERATED_BODY()

public:
	UPROPERTY(config, EditAnywhere, Category = "Display Settings", meta = (
		DisplayName = "Layer Depth",
		ToolTip = "Spacing for each symbol in a PolyVector; used to avoid Z-fighting, but can also be used to intentionally break apart a PolyVector.",
		ConfigRestartRequired = false))
		float LayerDepth = 0.002;


	UPROPERTY(config, EditAnywhere, Category = "Quality Settings", meta = (
		DisplayName = "Maximum LOD",
		ToolTip = "The maximum LOD that will be displayed, regardless of distance to camera or any other settings.",
		ConfigRestartRequired = false))
		int8 MaxLod = 4;

	UPROPERTY(config, EditAnywhere, Category = "Quality Settings", meta = (
		DisplayName = "Maximum Number Of Shadow Casters",
		ToolTip = "The maximum number of shadow casting shapes per PolyVector.",
		ConfigRestartRequired = false))
		uint16 MaxShadowCasters = UINT16_MAX;

	UPROPERTY(config, EditAnywhere, Category = "Quality Settings", meta = (
		DisplayName = "Minimum Area For Shadow Caster",
		ToolTip = "The minimum area of a shape before it is considered for shadow casting.",
		ConfigRestartRequired = false))
		float MinAreaShadow = 0.0;

	UPROPERTY(config, EditAnywhere, Category = "Quality Settings", meta = (
		DisplayName = "LOD Distance Scale",
		ToolTip = "How many units away from the camera a PolyVector can be before quality begins to decrease.",
		ConfigRestartRequired = false))
		float LodDistanceScale = 100.0;

	UPROPERTY(config, EditAnywhere, Category = "Quality Settings", meta = (
		DisplayName = "LOD Distance Curve",
		ToolTip = "Applies a curve to the LOD Distance Scale.",
		ConfigRestartRequired = false))
		float LodDistanceCurve = 0.55;

	UPROPERTY(config, EditAnywhere, Category = "Quality Settings", meta = (
		DisplayName = "LOD Epsilon",
		ToolTip = "Delta value to use when reducing the point count in a PolyVector for negative quality values.",
		ConfigRestartRequired = false))
		float LodEpsilon = 0.025;

public:
	virtual void PostInitProperties() override;
	virtual FName GetCategoryName() const override;

#if WITH_EDITOR
	virtual void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent) override; 
#endif
};
