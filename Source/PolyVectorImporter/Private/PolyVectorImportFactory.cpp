/**
 * Copyright ©2019 BattyBovine
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 **/


#include "PolyVectorImportFactory.h"
#include "PolyVectorAsset.h"
#include "PolyVectorImporter.h"

#include "Editor/UnrealEd/Public/Editor.h"
#include "EditorFramework/AssetImportData.h"
#include "Engine/StaticMesh.h"
#include "Misc/FeedbackContext.h"
#include "Misc/FileHelper.h"
#include "Misc/Paths.h"
#include "Policies/CondensedJsonPrintPolicy.h"
#include "Serialization/ArrayReader.h"
#include "Serialization/JsonReader.h"
#include "Serialization/JsonSerializer.h"


#define LOCTEXT_NAMESPACE "PolyVectorFactory"


UPolyVectorImportFactory::UPolyVectorImportFactory(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	this->bCreateNew = false;
	this->bEditAfterNew = false;
	this->bEditorImport = true; // binary / general file source
	this->bText = false; // text source (maybe support?)

	this->SupportedClass = UPolyVectorAsset::StaticClass();

	Formats.Add(FString(TEXT("swf;")) + NSLOCTEXT("UPolyVectorAssetFactory", "FormatShockwaveFlash", "Shockwave Flash").ToString());
}

bool UPolyVectorImportFactory::FactoryCanImport(const FString& Filename)
{
	bool bCanImport = false;

	const FString Extension = FPaths::GetExtension(Filename);
	if (Extension == TEXT("swf"))
	{
		return true;
		//FArchive* Reader = IFileManager::Get().CreateFileReader(*Filename);
		//if (Reader)
		//{
		//	TArray<uint8> Data;
		//	Reader->Serialize(Data.GetData(), 3);
		//	bCanImport = IsHeaderValid(Data);
		//	Reader->Close();
		//	delete Reader;
		//}
	}

	return bCanImport;
}

UObject* UPolyVectorImportFactory::FactoryCreateFile(UClass* InClass, UObject* InParent, FName InName, EObjectFlags Flags, const FString& Filename, const TCHAR* Parms, FFeedbackContext* Warn, bool& bOutOperationCanceled)
{
	GEditor->GetEditorSubsystem<UImportSubsystem>()->BroadcastAssetPreImport(this, InClass, InParent, InName, Parms);

	Warn->Log(CurrentFilename);
	const FString Extension = FPaths::GetExtension(CurrentFilename);
	const FString Path = FPaths::GetPath(CurrentFilename);

	if (Extension == TEXT("swf"))
	{
		FPolyVectorImporter Importer;

		TArray<uint8> Data;
		if (FFileHelper::LoadFileToArray(Data, *CurrentFilename))
		{
			UPolyVectorAsset* PolyVectorAsset = NewObject<UPolyVectorAsset>(InParent, InClass, InName, Flags);
			switch (Importer.ImportSWFData(Data, PolyVectorAsset))
			{
				case EPolyVectorImportError::NullData:
					Warn->Log(TEXT("File contains null data"));
					return nullptr;
				case EPolyVectorImportError::NoZLIB:
					Warn->Log(TEXT("Cannot import zlib compressed file; zlib is not compiled into the plugin"));
					return nullptr;
				case EPolyVectorImportError::NoLZMA:
					Warn->Log(TEXT("Cannot import LZMA compressed file; LZMA is not compiled into the plugin"));
					return nullptr;
				case EPolyVectorImportError::EncryptedSWF:
					Warn->Log(TEXT("A password is required to decrypt this file"));
					return nullptr;
				case EPolyVectorImportError::InvalidSWFHeader:
					Warn->Log(TEXT("The header for this SWF file is malformed"));
					return nullptr;
			}
			PolyVectorAsset->AssetImportData->Update(CurrentFilename);
			return PolyVectorAsset;
		}
	}
	return nullptr;
}


bool UPolyVectorImportFactory::CanReimport(UObject * Obj, TArray<FString>& OutFilenames)
{
	if (Obj->GetClass() == UPolyVectorAsset::StaticClass())
	{
		UPolyVectorAsset* PolyVectorAsset = Cast<UPolyVectorAsset>(Obj);
		if (PolyVectorAsset && PolyVectorAsset->AssetImportData)
		{
			PolyVectorAsset->AssetImportData->ExtractFilenames(OutFilenames);
			if (OutFilenames.Num() >= 0)
			{
				this->PreferredReimportPath = OutFilenames[0];
			}
			return true;
		}
	}
	return false;
}

void UPolyVectorImportFactory::SetReimportPaths(UObject* Obj, const TArray<FString>& NewReimportPaths)
{
	if (Obj->GetClass() == UPolyVectorAsset::StaticClass())
	{
		UPolyVectorAsset* PolyVectorAsset = Cast<UPolyVectorAsset>(Obj);
		if (PolyVectorAsset && ensure(NewReimportPaths.Num() == 1))
		{
			PolyVectorAsset->AssetImportData->UpdateFilenameOnly(NewReimportPaths[0]);
		}
	}
}

EReimportResult::Type UPolyVectorImportFactory::Reimport(UObject* Obj)
{
	if (Obj->GetClass() != UPolyVectorAsset::StaticClass())
	{
		return EReimportResult::Failed;
	}

	UPolyVectorAsset* PolyVectorAsset = Cast<UPolyVectorAsset>(Obj);
	if (!PolyVectorAsset)
	{
		return EReimportResult::Failed;
	}

	this->CurrentFilename = this->PreferredReimportPath;
	TArray<uint8> Data;
	FFileHelper::LoadFileToArray(Data, *this->CurrentFilename);
	if (Data.Num() <= 0)
	{
		return EReimportResult::Failed;
	}
	FPolyVectorImporter Importer;
	Importer.ImportSWFData(Data, PolyVectorAsset);
	if (!PolyVectorAsset)
	{
		return EReimportResult::Failed;
	}

	return EReimportResult::Succeeded;
}

#undef LOCTEXT_NAMESPACE
