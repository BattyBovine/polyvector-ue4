/**
 * Copyright �2019 BattyBovine
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 **/


#include "PolyVectorImporter.h"

#include "PolyVectorAsset.h"
#include "PolyVectorAssetImportData.h"


EPolyVectorImportError FPolyVectorImporter::ImportSWFData(TArray<uint8>& Data, UPolyVectorAsset* PolyVectorAsset)
{
	if (this->IsHeaderValid(Data))
	{
		SWF::Parser *SWFParser = new SWF::Parser();
		switch (SWFParser->parse_swf_data(Data.GetData(), Data.Num()))
		{
			case SWF::Error::SWF_NULL_DATA:
				return EPolyVectorImportError::NullData;
			case SWF::Error::ZLIB_NOT_COMPILED:
				return EPolyVectorImportError::NoZLIB;
			case SWF::Error::LZMA_NOT_COMPILED:
				return EPolyVectorImportError::NoLZMA;
			case SWF::Error::SWF_FILE_ENCRYPTED:
				return EPolyVectorImportError::EncryptedSWF;
		}
		const SWF::Dictionary* SWFDictionary = SWFParser->get_dict();
		const SWF::Properties* SWFProperties = SWFParser->get_properties();

		this->BuildObject(PolyVectorAsset, SWFDictionary, SWFProperties);
		if (!PolyVectorAsset->AssetImportData || !PolyVectorAsset->AssetImportData->IsA<UPolyVectorAssetImportData>())
		{
			PolyVectorAsset->AssetImportData = NewObject<UPolyVectorAssetImportData>(PolyVectorAsset);
		}
		return EPolyVectorImportError::NoError;
	}

	return EPolyVectorImportError::InvalidSWFHeader;
}


bool FPolyVectorImporter::IsHeaderValid(TArray<uint8>& Data)
{
	const int64 FileSize = Data.Num();
	if (FileSize < 3)
	{
		return false;
	}

	// SWF files begin with a header containing one of:
	// - ZWS (lzma compression)
	// - CWS (zlib compression)
	// - FWS (no compression)
	const char Header[3] = { Data[0], Data[1], Data[2] };
	const bool bReturnValue =
		(Header[0] == 'Z' || Header[0] == 'C' || Header[0] == 'F') &&
		Header[1] == 'W' &&
		Header[2] == 'S';
	return bReturnValue;
}

bool FPolyVectorImporter::BuildObject(UPolyVectorAsset* Asset, const SWF::Dictionary* SWFDictionary, const SWF::Properties* SWFProperties)
{
	const float VectorScale = 0.5;	// 5.0 (scale factor) / 10.0;

	// Store animation properties at the root of the asset file
	Asset->FPS = SWFProperties->framerate;
	Asset->Dimensions = FVector2D(SWFProperties->dimensions.xmax, SWFProperties->dimensions.ymax);

	// Clear the label data in case it's already populated, and then get the new label data
	Asset->SceneLabelData.Empty(SWFDictionary->SceneLabelData.size());
	Asset->FrameLabelData.Empty(SWFDictionary->FrameLabelData.size());
	for (std::map<uint16, std::string>::const_iterator SceneLabelIterator = SWFDictionary->SceneLabelData.begin(); SceneLabelIterator != SWFDictionary->SceneLabelData.end(); SceneLabelIterator++)
	{
		Asset->SceneLabelData.Add(SceneLabelIterator->second.c_str(), SceneLabelIterator->first);
	}
	for (std::map<uint16, std::string>::const_iterator FrameLabelIterator = SWFDictionary->FrameLabelData.begin(); FrameLabelIterator != SWFDictionary->FrameLabelData.end(); FrameLabelIterator++)
	{
		Asset->FrameLabelData.Add(FrameLabelIterator->second.c_str(), FrameLabelIterator->first);
	}

	// Build the colour fill library
	Asset->Library.FillStyles.Empty(SWFDictionary->FillStyles.size());
	std::map<uint16, uint16> FillStyleMap;
	for (SWF::FillStyleMap::const_iterator FillStyleMapIterator = SWFDictionary->FillStyles.begin(); FillStyleMapIterator != SWFDictionary->FillStyles.end(); FillStyleMapIterator++)
	{
		FPolyVectorFillStyles FillStyles;
		for (SWF::FillStyleArray::const_iterator FillStyleIterator = FillStyleMapIterator->second.begin(); FillStyleIterator != FillStyleMapIterator->second.end(); FillStyleIterator++)
		{
			SWF::FillStyle FillStyle = *FillStyleIterator;
			FillStyleMap[FillStyleIterator - FillStyleMapIterator->second.begin() + 1] = FillStyleMap.size() + 1;
			if (FillStyle.StyleType == SWF::FillStyle::Type::SOLID)
			{
				FillStyles.Array.Push(FPolyVectorFillStyle(
					FillStyle.Color.r,
					FillStyle.Color.g,
					FillStyle.Color.b,
					FillStyle.Color.a
				));
			}
			else
			{
				// Placeholder for unsupported fill types
				FillStyles.Array.Push(FPolyVectorFillStyle());
			}
		}
		Asset->Library.FillStyles.Push(FillStyles);
	}

	//Asset->Library.LineStyles.Empty(SWFDictionary->LineStyles.size());
	//std::map<uint16, uint16> LineStyleMap;

	// Create the shape definitions
	Asset->Library.Characters.Empty(SWFDictionary->CharacterList.size());
	std::map<uint16, uint16> CharacterMap;
	for (SWF::CharacterDict::const_iterator CharacterIterator = SWFDictionary->CharacterList.begin(); CharacterIterator != SWFDictionary->CharacterList.end(); CharacterIterator++)
	{
		const uint16 CharacterID = CharacterIterator->first;
		if (CharacterID > 0)
		{
			SWF::Character Character = CharacterIterator->second;
			CharacterMap[CharacterID] = CharacterMap.size() + 1;
			FPolyVectorShapeCharacter CharacterDef;
			SWFPolygonList ShapeList = this->ShapeBuilder(Character.shapes);
			// Merge shapes from Flash into solid objects and calculate hole placement and fill rules
			for (SWFPolygonList::iterator Shape = ShapeList.begin(); Shape != ShapeList.end(); Shape++)
			{
				FPolyVectorShape ShapeOut;
				//if (Shape->Polygon.Layer > 0)
				//{
				//	ShapeOut.Layer = Shape->Polygon.Layer;
				//}
				ShapeOut.Fill = Shape->Fill;
				ShapeOut.Area = FMath::Abs(Shape->Area);
				//ShapeOut.Stroke = Shape->Stroke;
				// If the area is negative, reverse the points so our winding is always consistent
				if (Shape->Area < 0)
				{
					this->PointsReverse(&Shape->Polygon);
				}
				TArray<float> Vertices;
				for (std::vector<SWF::Vertex>::iterator Vertex = Shape->Polygon.vertices.begin(); Vertex != Shape->Polygon.vertices.end(); Vertex++)
				{
					if (Vertex != Shape->Polygon.vertices.begin())
					{
						Vertices.Push(-Vertex->control.x * VectorScale);
						Vertices.Push(-Vertex->control.y * VectorScale);
					}
					if (Shape->Polygon.closed == true && Vertex == (Shape->Polygon.vertices.end() - 1))
					{
						break;
					}
					Vertices.Push(-Vertex->anchor.x * VectorScale);
					Vertices.Push(-Vertex->anchor.y * VectorScale);
				}
				ShapeOut.Path = this->VertsToCurve(Vertices, Shape->Polygon.closed);
				TArray<uint16> Holes;
				for (std::list<uint16>::iterator Hole = Shape->Children.begin(); Hole != Shape->Children.end(); Hole++)
				{
					if (ShapeList[*Hole].Fill == 0)
					{
						ShapeOut.Holes.Push(*Hole);
					}
				}
				CharacterDef.Shapes.Push(ShapeOut);
			}
			// Sort shapes from largest to smallest. so they can be easily displayed in the correct order
			CharacterDef.Shapes.Sort([this](const FPolyVectorShape& L, const FPolyVectorShape& R)
			{
				return FPolyVectorImporter::ShapeArea(L) > FPolyVectorImporter::ShapeArea(R);
			});
			CharacterDef.Bounds.Min = FVector2D(-Character.bounds.xmin * VectorScale, -Character.bounds.ymin * VectorScale);
			CharacterDef.Bounds.Max = FVector2D(-Character.bounds.xmax * VectorScale, -Character.bounds.ymax * VectorScale);
			Asset->Library.Characters.Push(CharacterDef);
		}
	}

	// Build display lists for each frame
	Asset->Frames.Empty(SWFDictionary->Frames.size());
	for (SWF::FrameList::const_iterator FrameIterator = SWFDictionary->Frames.begin(); FrameIterator != SWFDictionary->Frames.end(); FrameIterator++)
	{
		SWF::DisplayList FrameDisplayList = *FrameIterator;
		FPolyVectorFrameDisplayList DisplayList;
		for (SWF::DisplayList::iterator DisplayListIterator = FrameDisplayList.begin(); DisplayListIterator != FrameDisplayList.end(); DisplayListIterator++)
		{
			SWF::DisplayChar &DisplayCharacter = DisplayListIterator->second;
			if (DisplayCharacter.id > 0)
			{
				FPolyVectorSymbol CharOut;
				CharOut.ID = CharacterMap[DisplayCharacter.id - 1];
				CharOut.Depth = DisplayListIterator->first;
				CharOut.ClipDepth = DisplayCharacter.clipdepth;

				CharOut.Transform.Translation = FVector2D(
					-DisplayCharacter.transform.TranslateX * VectorScale,
					-DisplayCharacter.transform.TranslateY * VectorScale
				);
				CharOut.Transform.Scale = FVector2D(
					DisplayCharacter.transform.ScaleX,
					DisplayCharacter.transform.ScaleY
				);
				CharOut.Transform.Skew = FVector2D(
					DisplayCharacter.transform.RotateSkew0,
					DisplayCharacter.transform.RotateSkew1
				);

				CharOut.ColourTransform.Add = FColor(
					DisplayCharacter.colourtransform.RedAddTerm,
					DisplayCharacter.colourtransform.GreenAddTerm,
					DisplayCharacter.colourtransform.BlueAddTerm,
					DisplayCharacter.colourtransform.AlphaAddTerm
				);
				CharOut.ColourTransform.Multiply = FLinearColor(
					DisplayCharacter.colourtransform.RedMultTerm,
					DisplayCharacter.colourtransform.GreenMultTerm,
					DisplayCharacter.colourtransform.BlueMultTerm,
					DisplayCharacter.colourtransform.AlphaMultTerm
				);

				DisplayList.Symbols.Push(CharOut);
			}
		}
		Asset->Frames.Push(DisplayList);
	}

	return true;
}

FPolyVectorImporter::SWFPolygonList FPolyVectorImporter::ShapeBuilder(SWF::ShapeList ShapeList)
{
	SWFPolygonList ShapeParts;
	ShapeParts.reserve(ShapeList.size());

	// Collect already closed shapes first
	for (SWF::ShapeList::iterator Shape = ShapeList.begin(); Shape != ShapeList.end(); Shape++)
	{
		if (Shape->closed)
		{
			SWFPolygon ShapePart;
			ShapePart.Polygon = *Shape;
			ShapePart.Area = this->ShapeArea(ShapePart.Polygon);
			if (!this->ShapeAreaTooSmall(ShapePart.Area))
			{
				// Use left fill if the shape is wound counter-clockwise
				if (ShapePart.Area < 0)
				{
					ShapePart.Fill = ShapePart.Polygon.fill0;
				}
				// Use right fill if the shape is wound clockwise
				else if (ShapePart.Area > 0)
				{
					ShapePart.Fill = ShapePart.Polygon.fill1;
				}
				// If the area actually happens to be exactly 0, ignore this shape
				else
				{
					continue;
				}
				ShapePart.Stroke = ShapePart.Polygon.stroke;
				ShapeParts.push_back(ShapePart);
			}
		}
	}

	// Sort from smallest to largest
	std::sort(ShapeParts.begin(), ShapeParts.end(),
		[](const SWFPolygon &a, const SWFPolygon &b) { return abs(a.Area) < abs(b.Area); });

	std::map<SWFPolygonList::iterator, std::list<SWF::ShapeList::iterator> > ChildSegments;
	std::set<SWFPolygonList::iterator> DiscardedPolygons;
	std::set<SWF::ShapeList::iterator> DiscardedSegments;
	// For every closed polygon...
	for (SWFPolygonList::iterator Outer = ShapeParts.begin(); Outer != ShapeParts.end(); Outer++)
	{
		// ...find the child polygons (i.e. holes)...
		for (SWFPolygonList::iterator Inner = ShapeParts.begin(); Inner != ShapeParts.end(); Inner++)
		{
			if (Outer == Inner || !Inner->Polygon.closed ||
				DiscardedPolygons.find(Inner) != DiscardedPolygons.end() ||
				!this->ShapeContainsPoint(Inner->Polygon.vertices.front().anchor, Outer->Polygon))
			{
				continue;
			}
			Outer->Children.push_back(Inner - ShapeParts.begin());
			Inner->HasParent = true;
			DiscardedPolygons.insert(Inner);
		}
		// ...and then inner line segments
		for (SWF::ShapeList::iterator Inner = ShapeList.begin(); Inner != ShapeList.end(); Inner++)
		{
			if (Inner->closed || DiscardedSegments.find(Inner) != DiscardedSegments.end() ||
				!this->ShapeContainsPoint(Inner->vertices.front().anchor, Outer->Polygon))
			{
				continue;
			}
			ChildSegments[Outer].push_back(Inner);
			DiscardedSegments.insert(Inner);
		}
	}

	// Shapes that get used to satisfy fill rules are discarded here
	std::set<SWF::ShapeList::iterator> Left, Right;
	// Merge line segments into complete shapes
	for (SWFPolygonList::iterator Parent = ShapeParts.begin(); Parent != ShapeParts.end(); Parent++)
	{
		for (std::list<SWF::ShapeList::iterator>::iterator LineIterator = ChildSegments[Parent].begin(); LineIterator != ChildSegments[Parent].end(); LineIterator++)
		{
			// Check to the left of the shape first (counter-clockwise)
			SWF::ShapeList::iterator Line = *LineIterator;
			if (Left.find(Line) == Left.end() && Line->fill0 != Parent->Polygon.fill0)
			{
				SWFPolygon ShapePart;
				ShapePart.Polygon = *Line;
				this->FindConnectedShapes(&ShapePart.Polygon, Line, false, Left, Right, ChildSegments[Parent]);
				if (ShapePart.Polygon.closed)
				{
					ShapePart.Area = this->ShapeArea(ShapePart.Polygon);
					if (!this->ShapeAreaTooSmall(ShapePart.Area))
					{
						ShapePart.Fill = ShapePart.Polygon.fill0;
						ShapePart.Stroke = ShapePart.Polygon.stroke;
						Parent->Children.push_back(ShapeParts.size());
						ShapePart.HasParent = true;
						ShapeParts.push_back(ShapePart);
					}
				}
			}
			// Then to the right (clockwise)
			if (Right.find(Line) == Right.end() && Line->fill1 != Parent->Polygon.fill0)
			{
				SWFPolygon ShapePart;
				ShapePart.Polygon = *Line;
				this->FindConnectedShapes(&ShapePart.Polygon, Line, true, Left, Right, ChildSegments[Parent]);
				if (ShapePart.Polygon.closed)
				{
					ShapePart.Area = this->ShapeArea(ShapePart.Polygon);
					if (!this->ShapeAreaTooSmall(ShapePart.Area))
					{
						ShapePart.Fill = ShapePart.Polygon.fill1;
						ShapePart.Stroke = ShapePart.Polygon.stroke;
						Parent->Children.push_back(ShapeParts.size());
						ShapePart.HasParent = true;
						ShapeParts.push_back(ShapePart);
					}
				}
			}
		}
	}

	// From here, find outside edges made from line segments that we've missed thus far
	// If line segments that have no parents exist at this point, these are our actual outside edges
	SWFPolygonList ShapeAdd;
	std::list<SWF::ShapeList::iterator> OuterSegments;
	for (SWF::ShapeList::iterator Line = ShapeList.begin(); Line != ShapeList.end(); Line++)
	{
		if (!Line->closed && DiscardedSegments.find(Line) == DiscardedSegments.end())
		{
			OuterSegments.push_back(Line);
		}
	}
	for (std::list<SWF::ShapeList::iterator>::iterator LineSegment = OuterSegments.begin(); LineSegment != OuterSegments.end(); LineSegment++)	// Make complete shapes from these segments
	{
		SWF::ShapeList::iterator Line = *LineSegment;
		if (!Line->closed && DiscardedSegments.find(Line) == DiscardedSegments.end())
		{
			// Check to the left of the shape first (counter-clockwise)
			if (Left.find(Line) == Left.end() && Line->fill0 != 0)
			{
				SWFPolygon ShapePart;
				ShapePart.Polygon = *Line;
				this->FindConnectedShapes(&ShapePart.Polygon, Line, false, Left, Right, OuterSegments);
				if (ShapePart.Polygon.closed)
				{
					ShapePart.Area = this->ShapeArea(ShapePart.Polygon);
					if (!this->ShapeAreaTooSmall(ShapePart.Area))
					{
						ShapePart.Fill = ShapePart.Polygon.fill0;
						ShapePart.Stroke = ShapePart.Polygon.stroke;
						ShapeAdd.push_back(ShapePart);
					}
				}
			}
			// Then to the right (clockwise)
			if (Right.find(Line) == Right.end() && Line->fill1 != 0)
			{
				SWFPolygon ShapePart;
				ShapePart.Polygon = *Line;
				this->FindConnectedShapes(&ShapePart.Polygon, Line, true, Left, Right, OuterSegments);
				if (ShapePart.Polygon.closed)
				{
					ShapePart.Area = this->ShapeArea(ShapePart.Polygon);
					if (!this->ShapeAreaTooSmall(ShapePart.Area))
					{
						ShapePart.Fill = ShapePart.Polygon.fill1;
						ShapePart.Stroke = ShapePart.Polygon.stroke;
						ShapeAdd.push_back(ShapePart);
					}
				}
			}
		}
	}

	// Sort from smallest to largest
	std::sort(ShapeAdd.begin(), ShapeAdd.end(),
		[](const SWFPolygon &a, const SWFPolygon &b) { return abs(a.Area) < abs(b.Area); });

	// Find new shapes that share a fill with their parent and remove them
	std::set<SWFPolygonList::iterator> DeleteNewShapes, DeleteOldShapes;
	for (SWFPolygonList::iterator Outer = ShapeAdd.begin(); Outer != ShapeAdd.end(); Outer++)
	{
		for (SWFPolygonList::iterator Inner = ShapeAdd.begin(); Inner != ShapeAdd.end(); Inner++)
		{
			if (Outer != Inner && DeleteNewShapes.find(Inner) == DeleteNewShapes.end() &&
				this->ShapeContainsPoint(Inner->Polygon.vertices.front().anchor, Outer->Polygon) &&
				Outer->Fill == Inner->Fill)
			{
				DeleteNewShapes.insert(Inner);
				break;
			}
		}
		for (SWFPolygonList::iterator Inner = ShapeParts.begin(); Inner != ShapeParts.end(); Inner++)
		{
			if (Outer != Inner && DeleteOldShapes.find(Inner) == DeleteOldShapes.end() &&
				this->ShapeContainsPoint(Inner->Polygon.vertices.front().anchor, Outer->Polygon) &&
				Outer->Fill == Inner->Fill)
			{
				DeleteOldShapes.insert(Inner);
				break;
			}
		}
	}
	for (std::set<SWFPolygonList::iterator>::iterator Delete = DeleteNewShapes.begin(); Delete != DeleteNewShapes.end(); Delete++)
	{
		ShapeAdd.erase(*Delete);
	}
	for (std::set<SWFPolygonList::iterator>::iterator Delete = DeleteOldShapes.begin(); Delete != DeleteOldShapes.end(); Delete++)
	{
		ShapeParts.erase(*Delete);
	}

	// Merge our newly-found shapes into the previous list
	ShapeParts.insert(ShapeParts.end(), ShapeAdd.begin(), ShapeAdd.end());

	// Finally, find children for our new shapes
	const uint16 PartsSize = ShapeParts.size();
	for (SWFPolygonList::iterator Outer = ShapeParts.begin() + PartsSize; Outer != ShapeParts.end(); Outer++)
	{
		for (SWFPolygonList::iterator Inner = ShapeParts.begin(); Inner != ShapeParts.end(); Inner++)
		{
			// Only add a child if the found shape has no parent
			// A child can't have a larger area than its parent
			if (Inner != Outer && !Inner->HasParent && abs(Inner->Area) < abs(Outer->Area) &&
				this->ShapeContainsPoint(Inner->Polygon.vertices.front().anchor, Outer->Polygon))
			{
				Outer->Children.push_back(Inner - ShapeParts.begin());
				Inner->HasParent = true;
			}
		}
	}

	return ShapeParts;
}

bool FPolyVectorImporter::ShapeContainsPoint(const SWF::Point& InnerVertex, const SWF::Shape& OuterShape)
{
	const uint16 OuterVertexCount = OuterShape.vertices.size();
	const SWF::Vertex *OuterVertices = &OuterShape.vertices[0];
	bool Contained = false;
	for (uint16 OuterEdge = 1; OuterEdge < OuterVertexCount; OuterEdge++) {
		if ((OuterVertices[OuterEdge].anchor.y > InnerVertex.y) != (OuterVertices[OuterEdge - 1].anchor.y > InnerVertex.y) &&
			InnerVertex.x < (OuterVertices[OuterEdge - 1].anchor.x - OuterVertices[OuterEdge].anchor.x) * (InnerVertex.y - OuterVertices[OuterEdge].anchor.y) / (OuterVertices[OuterEdge - 1].anchor.y - OuterVertices[OuterEdge].anchor.y) + OuterVertices[OuterEdge].anchor.x)
			Contained = !Contained;
	}
	return Contained;
}

void FPolyVectorImporter::FindConnectedShapes(SWF::Shape *BuildShape, const SWF::ShapeList::iterator Shape, const bool Clockwise, std::set<SWF::ShapeList::iterator>& LeftUsed, std::set<SWF::ShapeList::iterator>& RightUsed, std::list<SWF::ShapeList::iterator>& ShapeList)
{
	std::list<SWF::ShapeList::iterator>::iterator PotentialNextShape;
	for (PotentialNextShape = ShapeList.begin(); PotentialNextShape != ShapeList.end(); PotentialNextShape++) {
		const SWF::ShapeList::iterator NextShape = *PotentialNextShape;
		if (NextShape == Shape || NextShape->closed)
			continue;
		if (this->PointsEqual(BuildShape->vertices.back(), NextShape->vertices.back()))	// If attached in reverse, compare opposite fills
		{
			if ((!Clockwise && BuildShape->fill0 == NextShape->fill1) ||
				(Clockwise && BuildShape->fill1 == NextShape->fill0))
				break;
		}
		else if (this->PointsEqual(BuildShape->vertices.back(), NextShape->vertices.front()))	// If attached in sequence, compare matching fills
		{
			if ((!Clockwise && BuildShape->fill0 == NextShape->fill0) ||
				(Clockwise && BuildShape->fill1 == NextShape->fill1))
				break;
		}
		else	// If the shape isn't attached, skip
		{
			continue;
		}
	}

	if (PotentialNextShape == ShapeList.end())	// If no connected shapes were found, this is the end
	{
		return;
	}

	SWF::ShapeList::iterator NextShape = *PotentialNextShape;
	SWF::Shape MergeShape = *NextShape;
	if (this->PointsEqual(BuildShape->vertices.back(), MergeShape.vertices.back()))
	{
		this->PointsReverse(&MergeShape);
		if (Clockwise)
		{
			LeftUsed.insert(NextShape);
		}
		else
		{
			RightUsed.insert(NextShape);
		}
	}
	else
	{
		if (Clockwise)
		{
			RightUsed.insert(NextShape);
		}
		else
		{
			LeftUsed.insert(NextShape);
		}
	}
	BuildShape->vertices.insert(BuildShape->vertices.end(), MergeShape.vertices.begin() + 1, MergeShape.vertices.end());

	if (this->PointsEqual(BuildShape->vertices.back(), BuildShape->vertices.front()))
	{
		BuildShape->closed = true;
	}
	else
	{
		this->FindConnectedShapes(BuildShape, NextShape, Clockwise, LeftUsed, RightUsed, ShapeList);
	}
}

bool FPolyVectorImporter::PointsEqual(const SWF::Vertex &A, const SWF::Vertex &B)
{
	return (
		FMath::RoundToInt(A.anchor.x * 20.0) == FMath::RoundToInt(B.anchor.x * 20.0) &&
		FMath::RoundToInt(A.anchor.y * 20.0) == FMath::RoundToInt(B.anchor.y * 20.0)
		);
}

void FPolyVectorImporter::PointsReverse(SWF::Shape* Shape)
{
	std::vector<SWF::Vertex> ReverseVerts;
	SWF::Point ControlCache;
	for (std::vector<SWF::Vertex>::reverse_iterator VertexIter = Shape->vertices.rbegin(); VertexIter != Shape->vertices.rend(); VertexIter++)
	{
		SWF::Point NewCtrl = ControlCache;
		ControlCache = VertexIter->control;
		VertexIter->control = NewCtrl;
		ReverseVerts.push_back(*VertexIter);
	}
	Shape->vertices = ReverseVerts;
	uint16 fill1 = Shape->fill0;
	Shape->fill0 = Shape->fill1;
	Shape->fill1 = fill1;
}

FORCEINLINE double FPolyVectorImporter::ShapeArea(const FPolyVectorShape Shape)
{
	if (Shape.Path.Curve.GetPointCount() < 3)
	{
		return 0.0;
	}

	TArray<FVector2D> ShapeData = Shape.Path.Curve.GetPositionData();
	if (!Shape.Path.Closed)
	{
		ShapeData.Emplace(ShapeData[0]);
	}

	const uint16 VertexCount = ShapeData.Num();
	double Area = 0.0;
	for (uint16 i = 1; i < VertexCount; i++)
	{
		Area += (ShapeData[i - 1].X * ShapeData[i].Y) - (ShapeData[i].X * ShapeData[i - 1].Y);
	}
	return (Area / 2.0);
}

FORCEINLINE double FPolyVectorImporter::ShapeArea(SWF::Shape Shape)
{
	if (Shape.vertices.size() < 3)
	{
		return 0.0;
	}
	if (!Shape.closed)
	{
		Shape.vertices.push_back(Shape.vertices.front());
	}
	const uint16 VertexCount = Shape.vertices.size();
	const SWF::Vertex* VertexArray = &Shape.vertices[0];
	double Area = 0.0;
	for (uint16 i = 1; i < VertexCount; i++)
	{
		Area += (VertexArray[i - 1].anchor.x * VertexArray[i].anchor.y) - (VertexArray[i].anchor.x * VertexArray[i - 1].anchor.y);
	}
	return (Area / 2.0);
}

FORCEINLINE FPolyVectorPath FPolyVectorImporter::VertsToCurve(TArray<float> Verts, bool Closed)
{
	FPolyVectorPath PVPath;
	PVPath.Closed = Closed;
	const uint32 VertsLen = Verts.Num();
	if (VertsLen > 2)
	{
		FVector2D InCtrlDelta(0.0, 0.0);
		FVector2D OutCtrlDelta(0.0, 0.0);
		FVector2D QuadControl(0.0, 0.0);
		FVector2D Anchor(Verts[0], Verts[1]);
		FVector2D FirstAnchor = Anchor;
		for (unsigned int i = 2; i < VertsLen; i++)
		{
			float Vert = Verts[i];
			switch ((i + 2) % 4)
			{
			case 0:
			{
				QuadControl.X = Vert;
				break;
			}
			case 1:
			{
				QuadControl.Y = Vert;
				OutCtrlDelta = (QuadControl - Anchor) * (2.0 / 3.0);
				PVPath.Curve.AddPoint(Anchor, InCtrlDelta, OutCtrlDelta);
				break;
			}
			case 2:
			{
				Anchor.X = Vert;
				break;
			}
			case 3:
			{
				Anchor.Y = Vert;
				InCtrlDelta = (QuadControl - Anchor) * (2.0 / 3.0);
				break;
			}
			}
		}
		if (Closed) {
			InCtrlDelta = (QuadControl - FirstAnchor) * (2.0 / 3.0);
			PVPath.Curve.AddPoint(FirstAnchor, InCtrlDelta, FVector2D(0.0, 0.0));
		}
	}
	return PVPath;
}
