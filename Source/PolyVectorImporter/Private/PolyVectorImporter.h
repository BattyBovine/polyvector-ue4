/**
 * Copyright �2019 BattyBovine
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 **/


#pragma once

#include "CoreMinimal.h"

#include <algorithm>
#include <map>
#include <set>
#include "libshockwave/swfparser.h"


enum EPolyVectorImportError : uint32
{
	NoError,
	NullData,
	InvalidSWFHeader,
	NoZLIB,
	NoLZMA,
	EncryptedSWF
};


/**
 * Importer functions for PolyVector files
 */
class POLYVECTORIMPORTER_API FPolyVectorImporter
{
public:
	FPolyVectorImporter(){}
	~FPolyVectorImporter(){}

	EPolyVectorImportError ImportSWFData(TArray<uint8>& Data, class UPolyVectorAsset* PolyVectorAsset);

private:
	struct SWFPolygon
	{
		SWF::Shape Polygon;
		double Area = 0.0;
		uint16_t Fill;
		uint16_t Stroke;
		bool HasParent = false;
		std::list<uint16> Children;
	};
	typedef std::vector<SWFPolygon> SWFPolygonList;

	bool IsHeaderValid(TArray<uint8>& Data);
	bool BuildObject(class UPolyVectorAsset* Asset, const SWF::Dictionary* SWFDictionary, const SWF::Properties* SWFProperties);
	SWFPolygonList ShapeBuilder(SWF::ShapeList ShapeList);
	bool ShapeContainsPoint(const SWF::Point& InnerVertex, const SWF::Shape& OuterShape);
	void FindConnectedShapes(SWF::Shape *BuildShape, const SWF::ShapeList::iterator Shape, const bool Clockwise, std::set<SWF::ShapeList::iterator>& LeftUsed, std::set<SWF::ShapeList::iterator>& RightUsed, std::list<SWF::ShapeList::iterator>& ShapeList);
	bool PointsEqual(const SWF::Vertex& A, const SWF::Vertex& B);
	void PointsReverse(SWF::Shape* Shape);
	FORCEINLINE static double ShapeArea(const struct FPolyVectorShape Shape);
	FORCEINLINE double ShapeArea(SWF::ShapeList::iterator ShapeIter) { return this->ShapeArea(*ShapeIter); }
	FORCEINLINE double ShapeArea(SWF::Shape Shape);
	FORCEINLINE bool ShapeAreaTooSmall(double a) { return (FMath::Abs(a) < KINDA_SMALL_NUMBER); }
	FORCEINLINE struct FPolyVectorPath VertsToCurve(TArray<float> Verts, bool Closed);
};
