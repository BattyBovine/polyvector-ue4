/**
 * Copyright ©2019 BattyBovine
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 **/

#include "CoreMinimal.h"

#include "AssetToolsModule.h"
#include "IPolyVectorImporter.h"
#include "Modules/ModuleManager.h"
#include "PolyVectorAssetActions.h"


#define LOCTEXT_NAMESPACE "FPolyVectorImportModule"

/**
 * PolyVector module implementation (private)
 */
class FPolyVectorImporterModule : public IPolyVectorImporter
{
public:
	virtual void StartupModule() override
	{
		IAssetTools& AssetTools = FModuleManager::LoadModuleChecked<FAssetToolsModule>("AssetTools").Get();
		this->PolyVectorAssetActions = MakeShareable(new FPolyVectorAssetActions);
		AssetTools.RegisterAssetTypeActions(PolyVectorAssetActions.ToSharedRef());
	}

	virtual void ShutdownModule() override
	{
		if (FModuleManager::Get().IsModuleLoaded("AssetTools"))
		{
			IAssetTools& AssetTools = FModuleManager::GetModuleChecked<FAssetToolsModule>("AssetTools").Get();
			if (this->PolyVectorAssetActions.IsValid())
			{
				AssetTools.UnregisterAssetTypeActions(this->PolyVectorAssetActions.ToSharedRef());
			}
		}
	}

private:
	TSharedPtr<IAssetTypeActions> PolyVectorAssetActions;
};

IMPLEMENT_MODULE(FPolyVectorImporterModule, PolyVectorImporter);


#undef LOCTEXT_NAMESPACE
