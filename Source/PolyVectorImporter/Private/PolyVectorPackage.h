/**
 * Copyright ©2019 BattyBovine
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 **/

#pragma once

#include "PackageTools.h"
#include "Misc/PackageName.h"
#include "ObjectTools.h"
#include "AssetToolsModule.h"

template <typename T>
UPackage* GetAssetPackageAndName(UObject* Parent, const FString& PreferredName, const TCHAR* BackupPrefix, FName BackupName, int32 BackupIndex, FString& OutName)
{
	FString AssetName;

	if (PreferredName.IsEmpty())
	{
		// Create asset name based on Prefix_Name_Index, e.g. "SM_Suzanne_0"
		AssetName = FString::Printf(TEXT("%s_%s_%d"), BackupPrefix, *BackupName.ToString(), BackupIndex);
	}
	else
	{
		AssetName = PreferredName;
	}

	AssetName = ObjectTools::SanitizeObjectName(AssetName);

	// set where to place the static mesh
	const FString BasePackageName = UPackageTools::SanitizePackageName(FPackageName::GetLongPackagePath(Parent->GetOutermost()->GetName()) / AssetName);

	const FString ObjectPath = BasePackageName + TEXT('.') + AssetName;
	T* ExistingAsset = LoadObject<T>(nullptr, *ObjectPath, nullptr, LOAD_Quiet | LOAD_NoWarn);

	UPackage* AssetPackage;

	if (ExistingAsset)
	{
		AssetPackage = ExistingAsset->GetOutermost();
	}
	else
	{
		const FString Suffix(TEXT("")); // empty

		static FAssetToolsModule& AssetToolsModule = FModuleManager::LoadModuleChecked<FAssetToolsModule>("AssetTools");
		FString FinalPackageName;
		AssetToolsModule.Get().CreateUniqueAssetName(BasePackageName, Suffix, FinalPackageName, AssetName);

		AssetPackage = CreatePackage(nullptr, *FinalPackageName);
	}

	OutName = AssetName;
	return AssetPackage;
}
