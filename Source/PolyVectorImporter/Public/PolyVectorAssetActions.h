/**
 * Copyright ©2019 BattyBovine
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 **/

#pragma once

#include "CoreMinimal.h"
#include "PolyVectorAsset.h"
#include "PolyVectorImportFactory.h"

#include "AssetTypeActions_Base.h"
#include "EditorReimportHandler.h"

//#include "PolyVectorAssetActions.generated.h"


class POLYVECTORIMPORTER_API FPolyVectorAssetActions : public FAssetTypeActions_Base
{
public:
	//FPolyVectorAssetActions(const TSharedRef<class ISlateStyle>& InStyle){}

	virtual FText GetName() const override
	{
		return NSLOCTEXT("AssetTypeActions", "PolyVectorAssetTypeActionsName", "PolyVector");
	}
	
	virtual UClass* GetSupportedClass() const override
	{
		return UPolyVectorAsset::StaticClass();
	}
	
	virtual FColor GetTypeColor() const override
	{
		return FColor::Turquoise;
	}
	
	virtual uint32 GetCategories() override
	{
		return EAssetTypeCategories::Media;
	}

	virtual bool IsImportedAsset() const override
	{
		return true;
	}
};
